/*
 * Copyright (C) 2017 Baidu, Inc. All Rights Reserved.
 */
package com.zhxy.classcard.face.utils;

import com.zhxy.classcard.face.exception.FaceError;

public interface OnResultListener<T> {
    void onResult(T result);

    void onError(FaceError error);
}
